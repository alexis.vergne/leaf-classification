# Leaf classification

This is a small project to compare different linear classification methods applied on the kaggle leaf dataset (https://www.kaggle.com/c/leaf-classification/data).

## Getting started

To test the different linear classification methods proposed here, just install the required packages in your working environment, then simply run `Script/Classification.py` from your favorite IDE.

## Results
The results obtained are subject to slight changes because of the random train/test split and the initialization of the algorithms. But the order of performance remains the same. As shown on figure 1, surprisingly, the baseline algorithm K-NN (for K Nearest Neighbor) works better than the logistic regression, which is supposed to be a more elaborated model. And without any surprise here, the model performing the best is the classification based on the support vector machine, which has been shown to be quite effective in the literature.

![](./Illustrations/ResultsClassification.png)

**fig. 1 - F-score obtained with the three methods tested here**
