##################################################
##### Leaf classification with linear models #####
##################################################

### Libraries ###
import pandas as pd
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import precision_score, recall_score, f1_score
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
import matplotlib.pyplot as plt


### Parameters ###
Data = '../Data/Dataset.csv'
UnknownLabelsData = '../Data/Test.csv'
RegLR = { 'C': np.logspace(-3, 3, 7), 'penalty':['l1','l2'] } # table of regularization parameters to look for during cross validation
RegSVC = { 'C': np.logspace(-3, 3, 7) } # table of regularization parameters to look for during cross validation
n_methods = 3

### Functions ###
def TrainTestKNN(X_train, X_test, y_train, y_test):
    # Getting the model
    KNN = KNeighborsClassifier()  # defining model
    KNN.fit(X_train, y_train)  # training
    # Getting the score
    KNNy_pred = KNN.predict(X_test)  # prediction
    KNNF1 = f1_score(y_test, KNNy_pred, average='micro')
    return(KNN, KNNF1)

def TrainTestMulticlassLR(X_train, X_test, y_train, y_test):
    # Getting the model
    MulticlassLR = OneVsRestClassifier(
        GridSearchCV(LogisticRegression(solver='liblinear', max_iter=1000000), RegLR, cv=5))  # defining model
    ## optimisation of the parameters of the models with grid search ##
    MulticlassLR.fit(X_train, y_train)  # training
    # Getting the score
    LRy_pred = MulticlassLR.predict(X_test)  # prediction
    LRF1 = f1_score(y_test, LRy_pred, average='micro')
    return(MulticlassLR, LRF1)

def TrainTestMulticlassSVC(X_train, X_test, y_train, y_test):
    # Getting the model
    MultiClassSVC = OneVsRestClassifier(GridSearchCV(LinearSVC(), RegSVC, cv=4))  # defining model
    ## optimisation of the parameters of the models with grid search ##
    MultiClassSVC.fit(X_train, y_train)  # training
    # Getting the score
    SVCy_pred = MultiClassSVC.predict(X_test)  # prediction
    SVCF1 = f1_score(y_test, SVCy_pred, average='micro')
    return(MultiClassSVC, SVCF1)

### Main program ###
## Importing data
RawData = pd.read_csv(Data)
print(RawData.head()) # taking a look to data format

X = RawData.iloc[:,2:] # getting the features (removing species name and id)
y = RawData.iloc[:,1] # getting the labels

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25)

## Baseline with K-NN
KNN, KNNF1 = TrainTestKNN(X_train, X_test, y_train, y_test)

## Comparing with logistic regression
MultiClassLR, LRF1 = TrainTestMulticlassLR(X_train, X_test, y_train, y_test)

## Comparing with SVC multiclass
MultiClassSVC, SVCF1 = TrainTestMulticlassSVC(X_train, X_test, y_train, y_test)

## Plotting F-Score of each tested method
FScore = [KNNF1, LRF1, SVCF1]

fig, ax = plt.subplots()
index = np.arange(n_methods)
bar_width = 0.4
opacity = 0.5

rects = plt.bar(index, FScore, bar_width, alpha=opacity, color='r')#, label='F-Score')

for rect in rects:
    height = rect.get_height()
    ax.text(rect.get_x() + rect.get_width()/2., 1.005*height, str(height)[:4], ha='center', va='bottom')

plt.xlabel('Method')
plt.ylabel('F-Scores')
plt.title('Scores by method')
plt.xticks(index, ('K-NN', 'Logistic Regression', 'Support Vector Classification'))
plt.legend()

plt.tight_layout()
plt.show()

## Selecting the best model
ModelsList = [KNN, MultiClassLR, MultiClassSVC]
print(FScore)
BestModel = ModelsList[FScore.index(max(FScore))]

TestData = pd.read_csv(UnknownLabelsData)
print(TestData.head()) # taking a look to data format

X = TestData.iloc[:,1:] # getting the features (removing species id)

y_pred = BestModel.predict(X)
y_pred = pd.DataFrame(y_pred, columns=['species'])

Res = pd.concat([y_pred, X], axis=1)
print(Res.head())